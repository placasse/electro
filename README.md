### Résumé  ###

* Ce projet contient un répertoire de petits projets arduino
  et un répertoire pour les schémas Fritzing correspondant.

### Utilisation ###

* Dans ProjetsArduinos, commencez par copiez Makefile.exemple dans Makefile.local
  et le personnaliser.

> cd ProjetsArduino

> cp Makefile.exemple Makefile.local

* Il faut bien sûr installer les outils d'arduino.
* Pour utiliser les makefile, il faut Arduino-Makefile.

> git clone https://github.com/arduino/Arduino.git

> git clone https://github.com/sudar/Arduino-Makefile.git

* Dans ProjetsFritzing, importer d'abord les composantes avant d'ouvrir les montages.

* Il faut avoir une version à jour de Fritzing

> git clone https://github.com/fritzing/fritzing-app


### Contributions ###

* Pour contribuez, faites simplement un pull request.

### Contact ###

* Patrick Lacasse

### ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)