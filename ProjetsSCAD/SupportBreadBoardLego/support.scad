//$fa=0.5; // default minimum facet angle
//$fs=2; // default minimum facet size
$fs=0.5; // default minimum facet size

include <breadboardPlate.scad>;
include <LegoPinConnectors.scad>;

// fr: On dépose le breadboard dans ce plateau, il tient à serre.
//     À chaque extérmité il y a une rangée de sept connecteur légo verticaux.
//     Le tout fait 13 unités légo de long.
// en: Breadboard hold tight in this basis.
//     There is seven vertical lego pin connectors each side.
//     Total lenght is 13 lego units.
module breadboardPlateWith2x7PinConnectors()
{

    // fr: Le plateau seul.
    // en: The plate alone.
    breadboardPlate();

    // fr: Sept connecteurs, orienté verticalement, à l'avant et l'arrière.
    // en: In front and back, seven vertical lego pin connectors.
    for(i = [-1,1])
    {
        translate([0, i*6*liftarm_Pitch, liftarm_Height/2 + bas_plateau_bb])
            rotate([0, -i*90, 90])
            stripOfVerticalPinConnector(7);
    }
}

// fr: Quatre cubes, deux de chaque bord sur la longueur du plateau, pour faire des connecteurs.
//     Placés pour pouvoir connecter le plateau à un boîtier de batteries Power Function.
// en: Four cubes, deux on each long side of the basis, will be drilled to make pin connectors.
//     Placed to fit on a Power Funtion batteries box.
module quatre_cube()
{
    for(i = [-1,1])
    {
        for(j = [-1,1])
        {
            epsilon = 0.001;
            translate([i*(4*liftarm_Pitch+epsilon), j*5*liftarm_Pitch, liftarm_Height/2 + bas_plateau_bb])
                cube([liftarm_Height, liftarm_Height, liftarm_Height], true);
        }
    }
}

// fr: On ajoute d'abord les quatres cubes au plateau, puis on perce les trous dans les cubes.
//     Les trous vont creusés un tout petit peu dans le plateau.
// en: The 4 cubes are added to the breadboard plate, then holes are drilled into the cubes.
//     Holes will penetrate a little bit into the plate.
difference()
{
    union()
    {
        breadboardPlateWith2x7PinConnectors();
        quatre_cube();
    }
    for(i = [-1,1])
    {
        for(j = [-1,1])
        {
            epsilon = 0.001;
            translate([i*(4*liftarm_Pitch+epsilon), j*5*liftarm_Pitch, liftarm_Height/2 + bas_plateau_bb])
                pinHole();
        }
    }
}
