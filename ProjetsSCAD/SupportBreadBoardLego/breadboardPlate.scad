

// fr: Les dimensions du breadboard : largeur, Longueur et hauteur.
//     Mesuré avec une règle en plastique, précision 0.5mm .
// en: Breadboard's dimension : width, lenght, height.
//     Measured with a plastic ruler, precision 0.5mm .

// breadBoard_Width
bb_l = 55;
// breadBoard_Length
bb_L = 82.5;
// breadBoard_Height
bb_h = 8.5;

// fr: Les petites pins pour connecter les breadboards ensemble.
//    Faites trop hautes pour que ça perce le plateau jusqu'en haut.
// en: There is little pins on sides of the breadboard to join them together.
//     With an excess to help place the breadboard in.
pin_d = 0.75;
pin_h = 4.5 + 5;

// fr: L'épaisseur des bord du plateau.
// en: Thickness of the walls of the plate.
// wall_thickness
parois = 3;

// fr: Le jeu tout le tour du breadboard.
// en: Space around the breadboard.
// free_space
tol = 1;

// fr: Les largeur,Longueur et hauteur totals du plateau.
// en: Total width, Length and height of this plate.
plateau_bb_l = bb_l + parois*2;
plateau_bb_L = bb_L + parois*2;
plateau_bb_h = bb_h + (2/3)*parois;
// fr: Pour les usagers, la coordonnée en z minimale de ce plateau.
// en: For user, z-coordinate minimal of this plate.
bas_plateau_bb = -plateau_bb_h/2 + bb_h/2 - parois;

module breadboardPlate()
{

    // fr: Le plateau est construit en plaçant d'abord un gros prisme rectangulaire
    //     auquel on retire le breadboard avec le jeu autour.
    // en: The plate is build of a box
    //     from which is removed the breadboard with some space around it.
    difference()
    {

        // fr: Le prisme qui sera creusé.
        // en: Main box which will be diged into.
        translate([0,0,bb_h/2 - parois])
            cube([plateau_bb_l, plateau_bb_L, plateau_bb_h], true);

        // fr: Un prisme pour le breadboard, fait un trou dans le plateau.
        // en: A box for the breadboard, this is removed from the plate.
        translate([0,0,bb_h/2])
            cube([bb_l + tol, bb_L + tol, bb_h],true);

        // fr: Il y a deux petits cylindres sur le côté long,
        //     et trois sur le côté court, pour l'accrocher à un autre.
        // en: There is two little cylinders on one long side,
        //     and three on one small side.
        translate([bb_l/2 + pin_d/2, 14 - bb_L/2, 0])
            cylinder(pin_h, pin_d + tol, pin_d + tol);
        translate([bb_l/2 + pin_d/2, 67 - bb_L/2, 0])
            cylinder(pin_h, pin_d + tol, pin_d + tol);
        translate([4.5 - bb_l/2, bb_L/2 + pin_d/2, 0])
            cylinder(pin_h, pin_d + tol, pin_d + tol);
        translate([27.5 - bb_l/2, bb_L/2 + pin_d/2, 0])
            cylinder(pin_h, pin_d + tol, pin_d + tol);
        translate([50 - bb_l/2, bb_L/2 + pin_d/2, 0])
            cylinder(pin_h, pin_d + tol, pin_d + tol);

    }

}

// fr: Décommenter pour tester.
// en: Uncomment to test.
//breadboardPlate();

