// fr: En attendant l'arrivé de mon vernier, j'ai utilisé les mesures provenant de ces sites :
// en: Until I receive my caliper, I used measure from those sites:
// http://bricks.stackexchange.com/questions/2066/what-are-the-size-of-lego-technic-parts
// http://www.thingiverse.com/thing:204466

liftarm_pinHoleDiameter = 4.8;
liftarm_pinHoleRadius = liftarm_pinHoleDiameter / 2;
liftarm_pinNotchDiameter = 6.1;
liftarm_pinNotchRadius = liftarm_pinNotchDiameter / 2;
liftarm_pinNotchDepth = 0.8;
liftarm_Height = 7.76;
liftarm_Width = 7.38;
liftarm_Pitch = 7.97;

// fr: Vraiment le trou du connecteur, à utiliser avec difference() pour percer les trous.
// en: Really the hole of connector, to be used with difference() to drill holes.
module pinHole(excess)
{
    union()
    {
        // fr: Le cylindre intérieur.
        // en: Interior cylinder.
        translate([-(liftarm_Height/2), 0, 0 ])
            rotate([0, 90, 0])
            cylinder(liftarm_Height, liftarm_pinHoleRadius, liftarm_pinHoleRadius);

        // fr: Une coche de chaque bord.
        //     J'ajoute un débordement, pour éviter une face d'épaisseur 0 dans openSCAD.
        // en: Each side a notch.
        //     An excess is added to avoid a face of thickness 0 in openSCAD.
        translate([(liftarm_Height/2) - liftarm_pinNotchDepth, 0, 0 ])
            rotate([0, 90, 0])
            cylinder(liftarm_pinNotchDepth + excess, liftarm_pinNotchRadius, liftarm_pinNotchRadius);
        translate([-(liftarm_Height/2 - liftarm_pinNotchDepth), 0, 0 ])
            rotate([0, -90, 0])
            cylinder(liftarm_pinNotchDepth + excess, liftarm_pinNotchRadius, liftarm_pinNotchRadius);
    }
}

module stripOfVerticalPinConnector(nbTrous)
{
    connecteur_L = liftarm_Width + liftarm_Pitch*(nbTrous - 1);

    difference()
    {
        // fr: On remplit d'abord une base dans laquelle percer les trous.
        // en: First fill a basis into which drill holes.
        union()
        {
            // fr: Un prisme en haut.
            // en: An upper box.
            translate([0, 0, liftarm_Pitch/4])
                cube([liftarm_Height, connecteur_L, liftarm_Pitch/2], true);

            // fr: Un gros cylindre à chaque bout.
            // en: A cylinder each side.
            for(i = [-1,1])
            {
                translate([-(liftarm_Height/2), i*(nbTrous - 1)*liftarm_Pitch/2, 0 ])
                    rotate([0, 90, 0])
                    cylinder(liftarm_Height, liftarm_Width/2, liftarm_Width/2);
            }

            // fr: Un prisme au milieu.
            // en: A box in middle.
            hr2 = liftarm_Pitch/2 + liftarm_Width/2;
            translate([0, 0, hr2/2 - liftarm_Width/2])
                cube([liftarm_Height, liftarm_Pitch*(nbTrous-1), hr2], true);
        }

        // fr: Une rangée de trou.
        // en: A strip of holes.
        for(i = [0:1:nbTrous-1])
        {
            y_translation = -(nbTrous-1)*liftarm_Pitch/2 + liftarm_Pitch*i;
            excess = 1;
            translate([0, y_translation, 0])
                pinHole(excess);

        }
    }
}

// fr: Décommenter pour tester.
// en: Uncomment to test.
//stripOfVerticalPinConnector(6);

// fr: Un cube avec un trou de connecteur.
// en: A cube with a connector hole.
module horizontalLegoPinConnector(hole_excess)
{
    difference()
    {
        cube([liftarm_Height, liftarm_Height, liftarm_Height], true);
        pinHole(hole_excess);
    }
}

// fr: Décommenter pour tester.
// en: Uncomment to test.
//horizontalLegoPinConnector();


