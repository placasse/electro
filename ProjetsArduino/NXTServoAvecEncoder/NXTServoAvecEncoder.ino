//
// Contrôle un moteur ev3 avec un PID.
//
//
// Un tour c'est 720 pas d'encodeur.
//

#include "Encoder.h"
#include "PatlacPID.h"
#include "Timer.h"

#define pinEncodeur1 2
#define pinEncodeur2 3
#define pinPWMavant   5
#define pinPWMarriere 6


Encoder encoder(pinEncodeur1, pinEncodeur2);
Timer monTimer;
PID monPID;

// Coefficient pour le ev3 seul.
// C'est bon aussi pour tourner les roues, mais sur le dos.
//const double Kp=0.8, Ki=1.0, Kd=0.3;

// Le ev3 qui tourne les roues sans avancer.
// Sans Kd, ça dépasse pas mal.
const double Kp=1.5, Ki=3.0, Kd=0.5;

const int frictionStatique  = 50;
const int frictionDynamique = 30;

int sortiePWM = 0;

int vitesseAvant = 0, vitesseArriere = 0;
int friction;

//
// Pour l'instant, je change de cible à chaque 5 secondes.
// Un quard de tour à droite, centre, un quart de tour à gauche.
//
void afficheInfo();
void changeCible();
void miseAjourPID();

void setup() {
  pinMode(pinPWMavant,   OUTPUT);
  pinMode(pinPWMarriere, OUTPUT);
  Serial.begin(9600);
  Serial.println("Début.");

  // Coefficient pour tourner les roues, mais sur le dos.
  monPID.asgnCoefficients(Kp, Ki, Kd);
  monPID.asgnTempsEntreMiseAjour(100);
  monPID.aErreurMaxPourIntegree = 60;

  monTimer.every(4000, changeCible);
  monTimer.every(25, miseAjourPID);

  changeCible();
}


void loop() {
  monPID.aEntree = encoder.read();
  monTimer.update();

  // Estimation de la friction.
  // Pour l'instant j'ai un coefficient de friction statique et un dynamique.
  const int sens = (monPID.aCible > monPID.aEntree ? 1 : (monPID.aCible < monPID.aEntree ? -1 : 0));
  friction = sens * (monPID.aDeltaEntree == 0 ? frictionStatique : frictionDynamique);

  // Estimation de l'inertie.
  // TODO.

  const int friction_plus_pid = monPID.aSortie + friction;
  if (friction_plus_pid > 0) {
    vitesseAvant = min(friction_plus_pid, 255);
    vitesseArriere = 0;
  } else if (monPID.aSortie < 0) {
    vitesseAvant = 0;
    vitesseArriere = min(-friction_plus_pid, 255);
  }

  analogWrite(pinPWMavant,   vitesseAvant);
  analogWrite(pinPWMarriere, vitesseArriere);
}

void miseAjourPID()
{
  monPID.miseAjour();
}

void changeCible()
{
  // Un tour = 720 ticks.
  // 1/2 tour = 360
  // 1/4 tour = 180
  // 1/8 tour = 90
  static int noCible = 1;
  switch (noCible % 3) {
    case 0 :
      monPID.aCible = 90;
      break;
    case 1 :
      monPID.aCible = 0;
      break;
    case 2 :
      monPID.aCible = -90;
      break;
  }
  ++noCible;
}

void afficheInfo()
{
  Serial.print("f ");
  Serial.print(friction);
  Serial.print(" v ");
  Serial.print(vitesseAvant);
  Serial.print(" ");
  Serial.println(vitesseArriere);
}
