#include <SoftwareSerial.h>

//
// Le bluetooth reçoit des commandes pour allumer les lumières :
//   R : Rouge
//   V : Vert
//   D : les Deux
//   E : Éteindre
//

// Le RX, TX du arduino doit être connecté
// au DO, DI du bluetooth.
SoftwareSerial lSerialBlueTooth(10, 11); // RX, TX
#define pinRouge 6
#define pinVerte 5

#define lDebug Serial


void setup()
{
  // La puce bluetooth HC-06 communique par défaut en 9600 bauds.
  lSerialBlueTooth.begin(9600);

  pinMode(pinRouge, OUTPUT);
  pinMode(pinVerte, OUTPUT);
}

bool lRougeAllumee = false;
bool lVerteAllumee = false;

void loop()
{
  char lCommande = 0;

  // On lit toutes les commandes reçues et on garde seulement la dernière.
  while (lSerialBlueTooth.available()) {
    // Une commande.
    lCommande = lSerialBlueTooth.read();
  }
  lDebug.print(lCommande);

  if (lCommande == 'R') {
    lRougeAllumee = true;
    lVerteAllumee = false;
  } else if (lCommande == 'V') {
    lRougeAllumee = false;
    lVerteAllumee = true;
  } else if (lCommande == 'D') {
    lRougeAllumee = true;
    lVerteAllumee = true;
  } else if (lCommande == 'E') {
    lVerteAllumee = false;
    lRougeAllumee = false;
  }

  digitalWrite(pinRouge, (lRougeAllumee?HIGH:LOW));
  digitalWrite(pinVerte, (lVerteAllumee?HIGH:LOW));
}

