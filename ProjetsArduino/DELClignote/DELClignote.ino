// La DEL hardwire sur les arduino (au moins promini et leonardo) est la pin 13.
#define ledPin 13

// La DEL de mon montage.
//#define ledPin 5

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // Allume.
  digitalWrite(ledPin, HIGH);

  // Attend une demie seconde.
  delay(500);

  // Éteint.
  digitalWrite(ledPin, LOW);

  // Attend.
  delay(500);
}
