
#include "PatlacPID.h"
#include "Arduino.h" // Pour Serial

PID::PID() :
  aEntree(0),
  aSortie(0),
  aCible(0),
  aErreurMaxPourIntegree(255),
  aKDecroissementIntegrale(0.8),
  aSortieMin(-255),
  aSortieMax(255),
  aKp(0.0),
  aKi(0.0),
  aKd(0.0),
  aKiSelonFrequence(0.0),
  aKdSelonFrequence(0.0),
  aTempsEntreMiseAjour(100),
  aErreurIntegree(0.0),
  aEntreePrec(0)
{
}

void PID::asgnCoefficients(float pKp, float pKi, float pKd)
{
  aKp = pKp;
  aKi = pKi;
  aKd = pKd;
  aKiSelonFrequence = aKi * (float)aTempsEntreMiseAjour / 1000.0;
  aKdSelonFrequence = aKd * 1000.0 / (float)aTempsEntreMiseAjour;
}

void PID::asgnTempsEntreMiseAjour(int pTempsEntreMiseAjour)
{
  aTempsEntreMiseAjour = pTempsEntreMiseAjour;
  aKiSelonFrequence = aKi * (float)aTempsEntreMiseAjour / 1000.0;
  aKdSelonFrequence = aKd * 1000.0 / (float)aTempsEntreMiseAjour;
}

void PID::miseAjour()
{
  const int lErreur = aCible - aEntree;
  // Calcule de la dérivée, pourrait utiliser un autre schéma que Euler explicite.
  // La division par le temps est déjà incorporée dans aKdSelonFrequence.
  aDeltaEntree = aEntree - aEntreePrec;
  aEntreePrec = aEntree;

  if (abs(lErreur) > aErreurMaxPourIntegree || lErreur == 0) {
    // Je ne veux pas garder le terme d'intégrale, ni l'enlever subitement.
    // Annuler l'intégrale lorsque l'erreur est nulle n'est peut-être pas pertinent pour toute les applications,
    // par exemple s'il faut forcer pour rester immobile.
    aErreurIntegree *= aKDecroissementIntegrale;
  } else {
    // La multiplication par le temps est déjà incorporée dans aKiSelonFrequence.
    aErreurIntegree += aKiSelonFrequence * lErreur;
  }

  double lSortie = aKp * lErreur + aErreurIntegree - aKdSelonFrequence * aDeltaEntree;

  const double lDebordementHaut = lSortie - aSortieMax;
  if (lDebordementHaut > 0) {
    lSortie = aSortieMax;
    // Évite d'accumulée une intégrale de plus en plus grande.
    aErreurIntegree -= lDebordementHaut;
  }
  const double lDebordementBas = lSortie - aSortieMin;
  if (lDebordementBas < 0.0) {
    lSortie = aSortieMin;
    // Évite d'accumuler une intégrale de plus en plus grande.
    aErreurIntegree -= lDebordementBas;
  }

  aSortie = lSortie;

  if (false)
  {
    Serial.print("c ");
    Serial.print(aCible);
    Serial.print(" e ");
    Serial.print(aEntree);
    Serial.print(" E ");
    Serial.print(lErreur);
    Serial.print(" d ");
    Serial.print(aDeltaEntree);
    Serial.print(" i ");
    Serial.print(aErreurIntegree);
    Serial.print(" s ");
    Serial.print(lSortie);
    Serial.print(" s ");
    Serial.println(aSortie);
  }
}
