#ifndef PIDPatlac_h
#define PIDPatlac_h

//
// Classe de PID (Pondération Intégrale Dérivée).
//
// Fortement inspirée de celle de brettbeauregard.com .
// L'algorithme en tant que tel est légèrement différent.
// Aussi je ne fais pas le timer moi-même, je laisse la classe Timer le faire.
//
class PID
{
public :
  PID();

  // Doit être appelée à intervalle régulier.
  void miseAjour();

  // Le temps en milli-secondes, séparant deux appels à miseAjour.
  // La fonction miseAjour doit être appelé à intervalle régulier.
  // Par défaut 100 ms, mais pour mes besoins 100ms c'est trop lent.
  void asgnTempsEntreMiseAjour(int pTempsEntreMiseAjour);

  // Les coefficients P I D
  void asgnCoefficients(float pKp, float pKi, float pKd);

void afficheDebug();

public :
  int aEntree;
  int aSortie;
  int aCible;

  // On intégre pas si l'erreur est plus grande que cette valeur :
  int aErreurMaxPourIntegree;

  float aKDecroissementIntegrale;

  // Les limites de la sortie :
  float aSortieMin;
  float aSortieMax;

  // Peut servir dans l'estimation de la friction.
  int aDeltaEntree;
private :

  // Les coefficients du PID :
  float aKp;
  float aKi;
  float aKd;

  float aKiSelonFrequence;
  float aKdSelonFrequence;


  int aTempsEntreMiseAjour;

  float aErreurIntegree;
  int aEntreePrec;
};

#endif // PIDPatlac_h
