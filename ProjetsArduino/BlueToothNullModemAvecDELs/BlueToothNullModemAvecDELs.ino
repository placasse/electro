#include <SoftwareSerial.h>
#include "Timer.h"
Timer monTimer;

//
// Transfert ce qui vient du SoftwareSerial (bluetooth) vers le Serial (usb) et vise-versa.
// Permet entre autre de chatter entre un port usb et un bluetooth.
//
// Si une donnée entre du bluetooth, la lumière verte allume un instant.
// Si une donnée entre du usb, la lumière rouge allume un instant.
//

// Le RX, TX du arduino doit être connecté
// au DO, DI du bluetooth.
SoftwareSerial lSerialBlueTooth(10, 11); // RX, TX

#define lSerialUSB Serial

#define pinDELbluetooth 5
#define pinDELusb 6

void EteintLED();

void setup()
{
  // La puce bluetooth HC-06 communique par défaut en 9600 bauds.
  // Pour connecter le bluetooth à un ordi on pourra faire :
  // > # Pour trouver la mac adresse du module bluetooth.
  // > hcitool scan
  // > # Pour créer le device /dev/rfcomm0 (en supposant qu'il n'existe pas déjà).
  // > sudo rfcomm bind rfcomm0 30:14:10:31:09:10
  // > # Pour ouvrir un terminal (miniterm est aussi populaire).
  // > screen /dev/rfcomm0 9600
  // Dans ubunut, il faut d'abord activer le bluetooth pour ce module dans le menu en haut à droite.
  lSerialBlueTooth.begin(9600);

  lSerialUSB.begin(115200);

  pinMode(pinDELusb, OUTPUT);
  pinMode(pinDELbluetooth, OUTPUT);

  monTimer.every(500, EteintLED);
}

void loop()
{
  char lValeur = 0;

  // On lit toutes les commandes reçues del'un pour l'écrire directement à l'autre.
  while (lSerialBlueTooth.available()) {
    lValeur = lSerialBlueTooth.read();
    lSerialUSB.print(lValeur);
    digitalWrite(pinDELbluetooth, HIGH);
  }
  while (lSerialUSB.available()) {
    lValeur = lSerialUSB.read();
    // Copie la valeur reçue du usb vers le bluetooth.
    lSerialBlueTooth.print(lValeur);
    // Echo
//    lSerialUSB.print(lValeur);
    digitalWrite(pinDELusb, HIGH);
  }

  monTimer.update();
}

void EteintLED()
{
    digitalWrite(pinDELusb, LOW);
    digitalWrite(pinDELbluetooth, LOW);
}
