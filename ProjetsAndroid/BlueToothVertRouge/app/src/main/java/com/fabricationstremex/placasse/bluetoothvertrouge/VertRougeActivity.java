package com.fabricationstremex.placasse.bluetoothvertrouge;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class VertRougeActivity extends AppCompatActivity {

    private VertRougeApplication mApp;
    private TextView mText;



    public void buttonGreenClick(View view){
        mApp.writeToBT("V");
    }

    public void buttonRedClick(View view){
        mApp.writeToBT("R");
//        mApp.mText.setText("Botton rouge cliqué.");
    }
    public void buttonCloseBTClick(View view){
        mApp.closeBT();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vert_rouge);

        mText = (TextView) findViewById(R.id.infoTextViewMain);
        mText.setText("VertRougeActivity onCreate");

        mApp = (VertRougeApplication) getApplication();
    }

    protected void onStart ()
    {
        super.onStart();
        mApp.mText = mText;
        mText.setText("VertRougeActivity onStart");
    }
}
