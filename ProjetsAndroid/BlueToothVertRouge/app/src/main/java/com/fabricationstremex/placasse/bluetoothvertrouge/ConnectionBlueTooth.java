package com.fabricationstremex.placasse.bluetoothvertrouge;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ConnectionBlueTooth extends AppCompatActivity {
    VertRougeApplication mApp;

    private TextView mText;


    public void buttonConnectClick(View view){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mText.setText("Recherche du bluetooth.");
            }
        });
        view.invalidate();
        if (mApp.startBT(this))
        {
            connectionEtablie(view);
        }
    }

    /**  */
    public void connectionEtablie(View view) {
        Intent intent = new Intent(this, VertRougeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_blue_tooth);

        mText = (TextView) findViewById(R.id.infoTextViewConnect);

        mApp = (VertRougeApplication) getApplication();
    }

    protected void onStart ()
    {
        super.onStart();
        mApp.mText = mText;
        mApp.closeBT();
        mText.setText("");
    }

}
