package com.fabricationstremex.placasse.bluetoothvertrouge;


import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 *
 */
public class VertRougeApplication extends Application {
    public BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mSocket;
    public BluetoothDevice mDevice;

    private OutputStream mOutputStream;

    public TextView mText;


    public void closeBT() {
        if (mSocket != null && mSocket.isConnected()) {
            try {
                mOutputStream.close();
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                mText.setText(R.string.BTCloseFailed);
            }

            mText.setText(getString(R.string.BTClosed));
        }
    }

    private void findBT(Activity activity) {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            mText.append("\n" + getString(R.string.BTUnsupported));
        } else {
//            mText.append("Bluetooth yé!");
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                int REQUEST_ENABLE_BT = 1;
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if(pairedDevices.size() > 0) {
                for(BluetoothDevice device : pairedDevices) {
//                    mText.append(device.getName());
                    if(device.getName().equals("monbleu")) {
                        mDevice = device;
//                        mText.append("Pairé. adresse: " + mDevice.getAddress() + "\n");
                        break;
                    }
                }
            }
        }
    }

    private void openBT() throws IOException {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard //SerialPortService ID
        mSocket = mDevice.createRfcommSocketToServiceRecord(uuid);
        mSocket.connect();
        mOutputStream = mSocket.getOutputStream();
    }

    public boolean startBT(Activity activity) {
//        mText.setText("startBT");
//        return false;

        if (mSocket == null || !mSocket.isConnected()) {
            try {
                findBT(activity);
                if (mDevice == null) {
                    mText.append("\n" + getString(R.string.BTNotPaired));
                    return false;
                } else {
                    openBT();
                }
                mText.setText(getString(R.string.BTOpened));
            } catch (IOException e) {
                mText.append("\n" + getString(R.string.BTFailed) + " " + e.getLocalizedMessage());
                return false;
            }
        }
        return true;
    }

    public void writeToBT(String msg) {
        if (mSocket == null || !mSocket.isConnected()) {
            mText.setText("La connection bluetooth est perdue.");
        }

        try {
            mOutputStream.write(msg.getBytes());
        } catch (IOException e) {
            mText.append("\n" + getString(R.string.BTWriteFailed) + e.getLocalizedMessage());
        }
        mText.append("." + msg + ".");
    }
}
